import requests
from bs4 import BeautifulSoup
import pandas as pd
import io

n = int(input(f"Choose team by current ranking on table: "))
n = n - 1

standings_url = "https://fbref.com/en/comps/9/Premier-League-Stats"
data = requests.get(standings_url)
print(data.status_code) # print status code. 429 = too many requests

soup = BeautifulSoup(data.text, features="lxml")
standings_table = soup.select('table.stats_table')[0]
links = standings_table.find_all('a')
links = [l.get("href") for l in links]
links = [l for l in links if '/squads/' in l]
team_urls = [f"https://fbref.com{l}" for l in links]
team_url = team_urls[n]
data = requests.get(team_url)

#matches = pd.read_html(data.text, match="Scores & Fixtures")
matches = pd.read_html(io.StringIO(data.text), match="Scores & Fixtures") # match details
#print(matches[0])

soup = BeautifulSoup(data.text, features="lxml")
links = soup.find_all('a')
links = [l.get("href") for l in links]
links = [l for l in links if l and 'all_comps/shooting/' in l]
data = requests.get(f"https://fbref.com{links[0]}")
shooting = pd.read_html(io.StringIO(data.text), match="Shooting")[0]
shooting.columns = shooting.columns.droplevel()

team_data = matches[0].merge(shooting[["Date", "Sh", "SoT", "Dist", "FK", "PK", "PKatt"]], on="Date") # merge matches with shooting

opponents = team_data["Opponent"] # print out specific column 

average_shots = team_data["Sh"].mean()
last_5_matches = team_data.tail(5)
average_shots_last_5 = last_5_matches["Sh"].mean()

print(f"Season Avg. Shots per game: {round(average_shots, 1)}")
print(f"Shots in last 5 matches: {round(average_shots_last_5, 1)}")
